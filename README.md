# Invoicer
Simple tool to generate `.pdf` invoices from Excel/CSV files.

## Requirements
1. [Python 3](https://www.python.org/)
2. [Miktex](https://miktex.org/) 

## Getting Started
_NOT IMPLEMENTED YET_

`python invoicer.py <input_filepath>`