# This file is part of Invoicer.
#
# Invoicer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Invoicer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Invoicer.  If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
import subprocess
import sys


def main(args):
    # CREATE THE OUTPUT DIRECTORY
    output_dir = Path.cwd().parents[2] / 'data'
    output_dir.mkdir(parents=True, exist_ok=True)
    tex_file = output_dir / 'out.tex'

    with open(tex_file, 'w') as f:
        f.write('\\documentclass{article}')
        f.write('\n\\begin{document}')
        f.write('\n\\section{TEST}')
        f.write('\nhelloworld')
        f.write('\n\\end{document}')
        
    subprocess.run(["pdflatex", str(tex_file), '-output-directory={}'.format(str(output_dir)), '-quiet'])
    

if __name__ == '__main__':
    main(sys.argv)
